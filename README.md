# README #

Refer: https://portal.nutanix.com/page/documents/details/?targetId=CSI-Volume-Driver-v1_1%3ACSI-Volume-Driver-v1_1

Prerequisites

(Nutanix Volumes only) 
	CentOS: iscsi-initiator-utils
	Red Hat Enterprise Linux (RHEL): iscsi-initiator-utils
	Ubuntu: open-iscsi
	
(Nutanix Files only) NFS mount packages:
	CentOS: nfs-utils
	RHEL: nfs-utils
	Ubuntu: nfs-common

Firewall Requirements
TCP Port	Source		Destination			Purpose
3260		Kubernetes VLAN	Prism Element	Uni-directional communication.
9440		Kubernetes VLAN	Prism Element	Uni-directional communication.

Procedure
1.Create a CSI Driver object, see Creating a CSI Driver Object
csi-driver.yaml
-------------------

apiVersion: storage.k8s.io/v1beta1
kind: CSIDriver
metadata:
  name: com.nutanix.csi
spec:
  attachRequired: false
  podInfoOnMount: true
-------------------
kubectl create -f csi-driver.yaml

2. Download the deployment YAML files, see Deploying Service Accounts.
wget http://download.nutanix.com/csi/v1.1.1/csi-v1.1.1.tar.gz
tar xvf csi-v1.1.1.tar.gz
kubectl create -f ntnx-csi-rbac.yaml
kubectl get serviceaccounts -n kube-system
kubectl get clusterrole -n kube-system
kubectl get clusterrolebinding -n kube-system

3. Deploy StatefulSet and DaemonSet using YAML files, see Deploying StatefulSet and DaemonSet.
kubectl create -f ntnx-csi-node.yaml
kubectl create -f ntnx-csi-provisioner.yaml
kubectl get pods -n kube-system

4. Manage storage, see Managing Storage. Update NFS server and shared path

sc.yaml
-------------------

kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
    name: acs-afs
provisioner: com.nutanix.csi
parameters:
    storageType: NutanixFiles
    nfsServer: 192.168.10.93
    nfsPath: /k8s
-------------------
kubectl create -f sc.yaml
kubectl get storageclass

5. Create a PersistentVolumeClaim, see Creating a Persistent Volume Claim.
claim.yaml
-------------------

kind: PersistentVolumeClaim
apiVersion: v1
metadata:
   name: claim2
spec:
   accessModes:
      - ReadWriteMany
   resources:
      requests:
         storage: 30Gi
   storageClassName: acs-afs

-------------------
kubectl create -f claim.yaml



